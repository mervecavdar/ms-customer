package com.mervecavdar.api;

import org.springframework.web.bind.annotation.*;

/**
 * Basic Restful Web Service Example
 */
@RestController
@RequestMapping("/api/v1/greeting")
public class GreetingController {

    @GetMapping("/hello1")
    public String getHello1() {
        return "GET - Hello1";
    }

    @GetMapping("/hello2")
    public String getHello2() {
        return "GET - Hello2";
    }

    @PostMapping("/hello2")
    public String postHello2() {
        return "POST - Hello2";
    }

    @PostMapping("/hello3")
    public String postHello3() {
        return "POST - Hello3";
    }

    @GetMapping("/hello4/{name}/{surname}")
    public String getHello4(@PathVariable("name") String name, @PathVariable("surname") String surname) {
        return "GET - Hello4: " + name + " " + surname;
    }

    @GetMapping("/hello5/{name}/{surname}")
    public String getHello5(@PathVariable String name, @PathVariable String surname) {
        return "GET - Hello5: " + name + " " + surname;
    }

    @GetMapping("/hello6")
    public String getHello6(@RequestParam String name, @RequestParam String surname) {
        return "GET - Hello6: " + name + " " + surname;
    }

    @GetMapping("/hello7/{name}")
    public String getHello7(@PathVariable String name, @RequestParam String surname) {
        return "GET Hello7: " + name + " " + surname;
    }

    @PostMapping("/hello8")
    public String postHello8(@RequestBody User user) {
        return "POST - Hello8: " + user;
    }

}
